
public class CalculadoraSt
{
	
	public Algoritmo func;
	
	public void setAlgoritmo(Algoritmo a)
	{
		this.func = a;
	}
	
	public void operacion(int a ,int b)
	{
		this.func.funcion(a,b);
	}
}
